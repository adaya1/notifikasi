﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Notifikasi.Services.Notif.Models
{
    public class NotifikasiModel
    {
        [JsonIgnore]
        public long id { get; set; }
        [JsonIgnore]
        public DateTime? CreatedAt { get; set; }
        public string AppKey { get; set; }
        public string Source { get; set; }
        public int Type { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string EmailRecipient { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string RefID { get; set; }
        public string UIType { get; set; }
        public string RedirectURI { get; set; }
        public int isRead { get; set; }
        public int isDelete { get; set; }
        public string Additional { get; set; }
        public string status { get; set; }
    }

    public class NotifikasiSearchModel
    {
        public string Source { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string Title { get; set; }
        public int isRead { get; set; }
        public int isDelete { get; set; }
    }

    public class getNotifModel
    {
        public int totalUnread { get; set; }
        public List<NotifikasiModel> top15Notif { get; set; }
    }


    public class SampleNotifModel
    {
        public string userID { get; set; }
        public string message { get; set; }
    }
    public class SampleNotifResponseModel
    {
        public string userID { get; set; }
        public string message { get; set; }
        public DateTime dateNow { get; set; }
        public string dateNowConverted { get; set; }

    }
}
