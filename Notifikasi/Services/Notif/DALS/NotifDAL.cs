﻿using Dapper;
using Notifikasi.Helpers;
using Notifikasi.Helpers.Dapper;
using Notifikasi.Helpers.Datatables;
using Notifikasi.Services.Notif.Interfaces;
using Notifikasi.Services.Notif.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;

namespace Notifikasi.Services.Notif.DALS
{
    public class NotifDal : INotif
    {
        private readonly IDapper _dapper;

        public NotifDal(IDapper dapper)
        {
            _dapper = dapper;
        }
        public async Task<DTPagination<NotifikasiModel>> GetListNotif(string AppKey, NotifikasiSearchModel search, string orderBy = "CreatedAt", int page_number = 1, int page_size = 10, SortEnum sort = SortEnum.DESC)
        {
            try
            {
                page_size = page_size <= 0 ? 10 : page_size;
                orderBy = string.IsNullOrEmpty(orderBy) ? "CreatedAt" : orderBy;

                
                var sql = $"SELECT * FROM {TableEnum.notification} where AppKey = '{AppKey}' ";

                if (!string.IsNullOrEmpty(search.Recipient))
                {
                    sql += $" and Recipient = '{search.Recipient?.Trim()}'";
                }

                if (search.isRead == 2)
                {
                    sql += " and isRead = 1";
                }
                if (search.isRead == 1)
                {
                    sql += " and isRead = 0";
                }

                var list_search_query = new List<string>();

                if (!string.IsNullOrEmpty(search.Source))
                {
                    list_search_query.Add($" Source LIKE '%{search.Source?.Trim()}%' ");
                }

                if (!string.IsNullOrEmpty(search.Sender))
                {
                    list_search_query.Add($" Sender LIKE '%{search.Sender?.Trim()}%' ");
                }

                if (!string.IsNullOrEmpty(search.Title))
                {
                    list_search_query.Add($" Title LIKE '%{search.Title?.Trim()}%' ");
                }
               

                if (list_search_query.Count() > 0)
                {
                    var search_query = " AND (";

                    for (int i = 0; i < list_search_query.Count; i++)
                    {
                        if (i > 0)
                        {
                            search_query += $" OR " + list_search_query[i];
                        }
                        else
                        {
                            search_query += list_search_query[i];
                        }
                    }

                    sql += search_query + ") ";
                }

                long count = await Task.FromResult(_dapper.CountAll(sql));
                if (orderBy.Equals("Message"))
                {
                    orderBy = "CAST(Message AS NVARCHAR(1000))";
                }
                sql += $" ORDER BY {orderBy} {sort.ToString()} OFFSET {page_number} ROWS FETCH NEXT {page_size} ROWS ONLY";
                var items = await Task.FromResult(_dapper.GetAll<NotifikasiModel>(sql));

                return DTPagination<NotifikasiModel>.Success(items, count, page_number, page_size);
            }
            catch (Exception ex)
            {
                return DTPagination<NotifikasiModel>.Failure(new List<string>() { ex.Message });
            }
        }

        
        public async Task<int> getNotifUnRead(string AppKey, string userid)
        {
            int returnVal = 0;
            try
            {
            var sql = $"SELECT * FROM {TableEnum.notification} where AppKey = '{AppKey}' and Recipient = '{userid}'  and isRead = 0 Order By CreatedAt Desc";
                returnVal = await Task.FromResult(_dapper.CountAll(sql));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnVal;
        }
        public async Task<NotifikasiModel> getNotifDetails(string AppKey, string id)
        {
            NotifikasiModel returnVal = new NotifikasiModel();
            try
            {
                var sql = $"SELECT * FROM {TableEnum.notification} where AppKey = @AppKey and id = @id";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id, System.Data.DbType.String);
                parameters.Add("@AppKey", AppKey, System.Data.DbType.String);
                returnVal = await Task.FromResult(_dapper.Get<NotifikasiModel>(sql, parameters));
                if (returnVal == null)
                {
                    returnVal = new NotifikasiModel();
                }
            }
            catch (Exception ex)
            {
                returnVal = new NotifikasiModel();
            }

            return returnVal;
        }

        public async Task<long> CreateNotif(NotifikasiModel payload)
        {
            long returnVal = 0;
            try
            {
                var sql = $"INSERT INTO {TableEnum.notification} " +
                   $"(CreatedAt, AppKey, Source, Type ,Sender, Recipient, EmailRecipient, Title, Message, RefID, UIType, RedirectURI, isRead, isDelete, Additional) " +
                   $"VALUES (@CreatedAt, @AppKey, @Source, @Type ,@Sender, @Recipient, @EmailRecipient, @Title, @Message, @RefID, @UIType, @RedirectURI, @isRead, @isDelete, @Additional);" +
                   $" SELECT CAST(SCOPE_IDENTITY() as int);";

                if(payload.Type == 0)
                {
                    payload.RedirectURI = "";
                }

                var parameters = new DynamicParameters();
                parameters.Add("@CreatedAt", DateTime.Now, System.Data.DbType.String);
                parameters.Add("@AppKey", payload.AppKey, System.Data.DbType.String);
                parameters.Add("@Source", payload.Source, System.Data.DbType.String);
                parameters.Add("@Type", payload.Type, System.Data.DbType.String);
                parameters.Add("@Sender", payload.Sender, System.Data.DbType.String);
                parameters.Add("@Recipient", payload.Recipient, System.Data.DbType.String);
                parameters.Add("@EmailRecipient", payload.EmailRecipient, System.Data.DbType.String);
                parameters.Add("@Title", payload.Title, System.Data.DbType.String);
                parameters.Add("@Message", payload.Message, System.Data.DbType.String);
                parameters.Add("@RefID", payload.RefID, System.Data.DbType.String);
                parameters.Add("@UIType", payload.UIType, System.Data.DbType.String);
                parameters.Add("@RedirectURI", payload.RedirectURI, System.Data.DbType.String);
                parameters.Add("@isRead", payload.isRead, System.Data.DbType.String);
                parameters.Add("@isDelete", payload.isDelete, System.Data.DbType.String);
                parameters.Add("@Additional", payload.Additional, System.Data.DbType.String);
                returnVal = await Task.FromResult(_dapper.Insert(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }

        public async Task<NotifikasiModel> UpdateStatusNotif(string AppKey, string id, string status)
        {
            NotifikasiModel returnVal = new NotifikasiModel();
            try
            {
                int isRead = 0;
                int isDelete = 0;
                if (status == "isRead")
                {
                    isRead = 1;
                }
                else if (status == "isNew")
                {
                    isRead = 0;
                    isDelete = 0;
                }
                else if (status == "isDeleted")
                {
                    isDelete = 1;
                }

                var sql = $"UPDATE {TableEnum.notification.ToString()} " +
                    $" SET isRead = @isRead, isDelete = @isDelete  where AppKey = @AppKey and id = @id";

                var parameters = new DynamicParameters();
                parameters.Add("@id", Convert.ToInt64(id), System.Data.DbType.Int64);
                parameters.Add("@AppKey", AppKey, System.Data.DbType.String);
                parameters.Add("@isRead", isRead, System.Data.DbType.String);
                parameters.Add("@isDelete", isDelete, System.Data.DbType.String);
                returnVal = await Task.FromResult(_dapper.Update<NotifikasiModel>(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }
      
        public async Task<List<NotifikasiModel>> getLengOfNotif(string AppKey, string length, string Recipient, string Source, string id)
        {
            List<NotifikasiModel> returnVal = new List<NotifikasiModel>();

            try
            {
                string additionalSearch = "";
                if (string.IsNullOrEmpty(id))
                {
                    additionalSearch = " And isRead = 0";
                }
                else
                {
                    additionalSearch = " And id =" + id;
                }
                var sql = $@"
                        SELECT TOP {length} *
                          FROM {TableEnum.notification.ToString()}  where 
                           AppKey = '{AppKey}' and Recipient = '{Recipient}'  {additionalSearch} 
                      ORDER BY [CreatedAt] DESC
                ";

                var parameters = new DynamicParameters();

                parameters.Add("@length", Convert.ToInt32(length), System.Data.DbType.Int32);
                returnVal = await Task.FromResult(_dapper.GetAll<NotifikasiModel>(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnVal;
        }

    }
}
