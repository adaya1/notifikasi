﻿using Notifikasi.Helpers;
using Notifikasi.Helpers.Datatables;
using Notifikasi.Services.Notif.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Notifikasi.Services.Notif.Interfaces
{
    public interface INotif
    {
        Task<DTPagination<NotifikasiModel>> GetListNotif(string AppKey, NotifikasiSearchModel search, string orderBy = "CreatedAt", int page_number = 1, int page_size = 10, SortEnum sort = SortEnum.DESC);
     
        Task<NotifikasiModel> getNotifDetails(string AppKey, string id);
       
        Task<int> getNotifUnRead(string AppKey, string userid);

        Task<long> CreateNotif(NotifikasiModel payload);

        Task<NotifikasiModel> UpdateStatusNotif(string AppKey, string id, string status);
        
        Task<List<NotifikasiModel>> getLengOfNotif(string AppKey, string length, string Recipient, string Source, string id);

    }
}
