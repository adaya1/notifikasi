﻿using Notifikasi.Helpers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Notifikasi.Services.Notif.Interfaces;
using Notifikasi.Services.Notif.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;
using Notifikasi.Hubs;

namespace Notifikasi.Controllers
{
    [Route("/notification")]
    public class NotifController : Controller
    {
        private readonly INotif iNotif;
        private readonly IHubContext<NotificationHub> notificationHubContext;

        public NotifController(INotif _iNotif, IHubContext<NotificationHub> _notificationContext)
        {
            iNotif = _iNotif;
            notificationHubContext = _notificationContext;
        }

        [HttpPost("getDatatables")]
        public async Task<IActionResult> getDatatables(
            [FromBody] NotifikasiSearchModel search,
            string AppKey,
            string orderBy,
            SortEnum sort,
            int pageNumber,
            int pageSize)
        {
            try
            {
                var data = await iNotif.GetListNotif(AppKey, search, orderBy, pageNumber, pageSize, sort);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }


        [HttpPost("sampleNotif")]
        public async Task<IActionResult> sampleNotif([FromBody] SampleNotifModel Data)
        {
            try
            {
                string msgSignalR = Data.message;
                await notificationHubContext.Clients.All.SendAsync("SendNotificationToUser", Data.userID, msgSignalR);
                SampleNotifResponseModel newData = new SampleNotifResponseModel();
                newData.message = Data.message;
                newData.userID = Data.userID;
                newData.dateNow = DateTime.Now;
                newData.dateNowConverted = DateTime.Now.ToString("dd-mm-yyyy HH:mm:ss");
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = newData
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }


        [HttpPost("pushNotif")]
        public async Task<IActionResult> createNotif([FromBody] NotifikasiModel Data)
        {
            try
            {
                Console.WriteLine(Data);
                var configApp = ConfigLoader.getSection("AppKeys");
                NotifikasiModel _data = new NotifikasiModel();

                string[] validApp = { configApp["EPM"], configApp["EBR"], configApp["ETESTING"] };
                if (!validApp.Contains(Data.AppKey))
                {
                    throw new Exception("Invalid app key");
                }
                else if (string.IsNullOrEmpty(Data.Source))
                {
                    throw new Exception("Requeired Data Source");
                }
                else if (string.IsNullOrEmpty(Data.Sender))
                {
                    throw new Exception("Requeired Data Sender");
                }
                else if (string.IsNullOrEmpty(Data.Recipient))
                {
                    throw new Exception("Requeired Data Recipient");
                }
                else if (string.IsNullOrEmpty(Data.Title))
                {
                    throw new Exception("Requeired Data Title");
                }
                else if (string.IsNullOrEmpty(Data.Message))
                {
                    throw new Exception("Requeired Data Message");
                }
                else
                {
                    var result = await iNotif.CreateNotif(Data);
                    if (result > 0)
                    {
                        _data = await iNotif.getNotifDetails(Data.AppKey, result.ToString());
                        string msgSignalR = Data.Title + "#" + Data.Message + "#" + Data.UIType + "#" + Data.RedirectURI + "#" + Data.RefID + "#" + Data.Sender + "#" + Data.Additional + "#" + Data.Type;
                        await notificationHubContext.Clients.All.SendAsync("SendNotificationToUser", Data.Recipient, msgSignalR);
                    }
                }

                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpPatch("updateNotif/{id}")]
        public async Task<IActionResult> updateNotif(string id, string AppKey, string status)
        {
            try
            {
                var configNotifikasis = ConfigLoader.getSection("AppKeys");
                string[] validApp = { configNotifikasis["EPM"], configNotifikasis["EBR"], configNotifikasis["ETESTING"] };
                string[] validStatus = { "isRead", "isNew", "isDeleted" };
                if (!validApp.Contains(AppKey))
                {
                    throw new Exception("Invalid app key");
                }
                else if (!validStatus.Contains(status))
                {
                    throw new Exception("Invalid status data");
                }
                else if (string.IsNullOrEmpty(id.ToString()))
                {
                    throw new Exception("Requeired Data id");
                }

                var _data = await iNotif.getNotifDetails(AppKey, id);
                var dataBefore = _data;
                if (_data.id == 0)
                {
                    throw new Exception("Record Not Found");
                }
                else
                {
                    _data = await iNotif.UpdateStatusNotif(AppKey, id, status);
                }

                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpGet("readNotif/{AppKey}/{id}")]
        public async Task<IActionResult> readNotif(string AppKey, string id)
        {
            try
            {
                var configNotifikasis = ConfigLoader.getSection("AppKeys");
                NotifikasiModel _data = new NotifikasiModel();

                string[] validApp = { configNotifikasis["EPM"], configNotifikasis["EBR"], configNotifikasis["ETESTING"] };
                if (!validApp.Contains(AppKey))
                {
                    throw new Exception("Invalid app key");
                }

                _data = await iNotif.getNotifDetails(AppKey, id);
                if (_data.id == 0)
                {
                    throw new Exception("Record Not Found");
                }
                else
                {
                    _data = await iNotif.UpdateStatusNotif(AppKey, id, "isRead");
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpGet("getUnread/{AppKey}/{userid}")]
        public async Task<IActionResult> getUnread(string AppKey, string userid)
        {
            try
            {
                var configNotifikasis = ConfigLoader.getSection("AppKeys");
                getNotifModel dataNotif = new getNotifModel();

                string[] validApp = { configNotifikasis["EPM"], configNotifikasis["EBR"], configNotifikasis["ETESTING"] };
                if (!validApp.Contains(AppKey))
                {
                    throw new Exception("Invalid app key");
                }

                dataNotif.totalUnread = await iNotif.getNotifUnRead(AppKey, userid);
                dataNotif.top15Notif = await iNotif.getLengOfNotif(AppKey, "15", userid, "", "");

                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = dataNotif
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpGet("getDetails/{AppKey}/{id}")]
        public async Task<IActionResult> getNotifById(string AppKey, string id)
        {
            try
            {
                var configNotifikasis = ConfigLoader.getSection("AppKeys");
                NotifikasiModel _data = new NotifikasiModel();

                string[] validApp = { configNotifikasis["EPM"], configNotifikasis["EBR"], configNotifikasis["ETESTING"] };
                if (!validApp.Contains(AppKey))
                {
                    throw new Exception("Invalid app key");
                }

                _data = await iNotif.getNotifDetails(AppKey , id);
                if (_data.id == 0)
                {
                    throw new Exception("Record Not Found");
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }


        [HttpGet("getDataNotif/{AppKey}/{length}")]
        public async Task<IActionResult> getLengOfNotif(string AppKey, string length, string Recipient, string Source, string id)
        {
            try
            {
                var configNotifikasis = ConfigLoader.getSection("AppKeys");
                List<NotifikasiModel> _data = new List<NotifikasiModel>();

                string[] validApp = { configNotifikasis["EPM"], configNotifikasis["EBR"], configNotifikasis["ETESTING"] };
                if (!validApp.Contains(AppKey))
                {
                    throw new Exception("Invalid app key");
                }
                _data = await iNotif.getLengOfNotif(AppKey, length, Recipient, Source, id);

                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

    }
}
