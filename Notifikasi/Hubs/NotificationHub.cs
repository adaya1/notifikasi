﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace Notifikasi.Hubs
{
    public class NotificationHub: Hub
    {
        public async Task SendNotificationToApp(string appId, string title, string message)
        {
            await Clients.Group(appId).SendAsync("NotificationReceive", title, message);
        }

        public async Task SendNotificationToUser(string title, string message)
        {
            Console.WriteLine("Sending to all: " + title);
            await Clients.All.SendAsync(title, message);
        }
    }
}
