﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace Notifikasi.Helpers
{
    public class ConfigLoader
    {
        public static IConfigurationSection getSection(string param)
        {
            var FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "")).Root;

            return new ConfigurationBuilder().AddJsonFile(FileProvider + "appsettings.json").Build().GetSection(param);

        }
    }
}
