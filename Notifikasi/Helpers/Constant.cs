﻿namespace Notifikasi.Helpers
{    public class Constant
    {
        public static string APP_NAME = "E-Notification";
        public static string APP_VERSION = "V.4.1.2022";
        public static string messageSuccess = "Success";
        public static string messageFailed = "Success";
        public static string messageExist = "Data already exists!";
        public static string messageNotExist = "Data not exists!";
        public static string codeSuccess = "1";
        public static string codeFailed = "-1";
        public static string codeDataNotValid = "422 ";
        public static string messageDataNotValid = "Data not valid!";


        public static string createData = "Create";
        public static string updateData = "Update";
        public static string deleteData = "Delete";

        public static string invalidTypeRequest = "Invalid request type!";
        public static string[] status_code_breakdown = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };


        #region 'Exchange & Queue Master RabbitMQ'
        public static string userCredentialMaster = "userCredential";
        public static string dataMachineMaster = "Ebridging-RA-Parent-Asset-Num";
        public static string dataWOMaster = "Ebridging-EAM-WO-OUT";
        public static string dataItemMaster = "Ebridging-M-Item";
        public static string dataInventoryPartMaster = "Ebridging-Inv-Item-Sparepart";
        #endregion

        #region 'Exchange & Queue Service'
        public static string userCredential = "userCredential_EPM_ServiceWo";
        public static string dataMachine = "Ebridging-RA-Parent-Asset-Num_EPM_ServiceWo";
        public static string dataWO = "Ebridging-EAM-WO-OUT_EPM_ServiceWo";
        public static string dataItem = "Ebridging-M-Item_EPM_ServiceWo";
        public static string dataInventoryPart = "Ebridging-Inv-Item-Sparepart_EPM_ServiceWo";
        #endregion


    }
}
