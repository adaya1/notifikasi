/*
 Navicat Premium Data Transfer

 Source Server         : MSSQL - ASP
 Source Server Type    : SQL Server
 Source Server Version : 11006020
 Source Host           : 103.25.195.236:1433
 Source Catalog        : Notification
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 11006020
 File Encoding         : 65001

 Date: 15/03/2022 12:42:55
*/


-- ----------------------------
-- Table structure for notification
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[notification]') AND type IN ('U'))
	DROP TABLE [dbo].[notification]
GO

CREATE TABLE [dbo].[notification] (
  [id] bigint  IDENTITY(1,1) NOT NULL,
  [CreatedAt] datetime  NOT NULL,
  [AppKey] varchar(150) COLLATE Latin1_General_CI_AS  NULL,
  [Source] varchar(50) COLLATE Latin1_General_CI_AS  NULL,
  [Type] int DEFAULT ((0)) NULL,
  [Sender] varchar(100) COLLATE Latin1_General_CI_AS  NULL,
  [Recipient] varchar(100) COLLATE Latin1_General_CI_AS  NULL,
  [Title] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [Message] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [RefID] bigint  NULL,
  [UIType] varchar(100) COLLATE Latin1_General_CI_AS  NULL,
  [RedirectURI] varchar(150) COLLATE Latin1_General_CI_AS  NULL,
  [isRead] int DEFAULT ((0)) NULL,
  [isDelete] int DEFAULT ((0)) NULL,
  [Additional] varchar(255) COLLATE Latin1_General_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[notification] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Primary Key structure for table notification
-- ----------------------------
ALTER TABLE [dbo].[notification] ADD CONSTRAINT [PK__notifica__3213E83FF400B6F7] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

